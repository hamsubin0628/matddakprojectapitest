package com.mj.matddakapi;

import io.jsonwebtoken.lang.Assert;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItem;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.UUID;

@SpringBootTest
class MatddakApiApplicationTests {

    @Test // 유닛 테스트 => @Test가 붙으면 하나의 유닛 테스트
    void contextLoads() {
        // 예시 데이터를 안 쪽에 바로 적어줘야 함
    }

//    @Test
//    void loadFileCheck() throws IOException {
//        File file = new File(new File("").getAbsoluteFile() + "/src/main/statics/02_study.png");
//        File fileItem = new DiskFileItem("originFile", Files.probeContentType(file.toPath()), false, file.getName());
//        String test = "1";
//    }

    @Test // 10줄 까지 테스트 하고 서비스로 옮기면 됨
    void  makeRandomName() {
        System.out.println("-----------------------------");
        System.out.println(UUID.randomUUID());
        System.out.println(LocalDateTime.now());
        System.out.println("-----------------------------");
    }

    @Test
    void testLogin() {
        String json = "{username: 'test', password: 'test'}";
    }
}
