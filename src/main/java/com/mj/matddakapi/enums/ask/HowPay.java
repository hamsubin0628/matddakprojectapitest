package com.mj.matddakapi.enums.ask;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum HowPay {
    CARD("카드")
    ,CASH("현금");

    private final String name;
}
