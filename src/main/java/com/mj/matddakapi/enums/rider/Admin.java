package com.mj.matddakapi.enums.rider;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Admin {
    ROLE_ADMIN("관리자")
    , ROLE_NORMAL("일반회원");

    private final String name;
}
