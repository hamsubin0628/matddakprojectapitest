package com.mj.matddakapi.interfaces;

public interface CommonModelBuilder<T>{
    T build();
}
