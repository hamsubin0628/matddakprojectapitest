package com.mj.matddakapi.repository;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.enums.rider.ReasonBan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RiderRepository extends JpaRepository<Rider, Long> {
    long countByEmail(String email);
    Optional<Rider> findByEmailAndPassword(String email,String password);

    List<Rider> findAllByIsBan(Boolean isBan);

    Optional<Rider> findByEmail(String email);
}