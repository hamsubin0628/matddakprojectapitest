package com.mj.matddakapi.lib;

public class CommonCheck {

    public static boolean checkEmail (String email) {
        String pattern = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\\\w+\\\\.)+\\\\w+$";
        return email.matches(pattern);
    }
}
