package com.mj.matddakapi.service;

import com.google.api.client.util.Value;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.mj.matddakapi.model.GCSRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GCSService {

    @Value("${spring.cloud.gcp.storage.bucket}")
    private String bucketName;

    public void uploadObject(GCSRequest gcsRequest) throws IOException {

        String keyFileName = "top-reef-413600-965eb8d313ab.json";
        InputStream keyFile = ResourceUtils.getURL("classpath:" + keyFileName).openStream();

        String uuid = UUID.randomUUID().toString();
        String ext = gcsRequest.getFile().getContentType();

        Storage storage = StorageOptions.newBuilder()
                .setCredentials(GoogleCredentials.fromStream(keyFile))
                .build()
                .getService();

        String imgUrl = "https://storage.googleapis.com/" + bucketName + "/" + uuid;

        BlobInfo blobInfo = BlobInfo.newBuilder(bucketName, gcsRequest.getFile().getOriginalFilename())
                .setContentType(gcsRequest.getFile().getContentType()).build();

        Blob blob = storage.create(blobInfo, gcsRequest.getFile().getInputStream());
    }
}
