package com.mj.matddakapi.service;

import com.mj.matddakapi.configure.JwtTokenProvider;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.enums.rider.Admin;
import com.mj.matddakapi.exception.CRiderMissingException;
import com.mj.matddakapi.model.rider.login.LoginRequest;
import com.mj.matddakapi.model.rider.login.LoginResponse;
import com.mj.matddakapi.repository.RiderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final RiderRepository riderRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    // 로그인 타입은 WEB or APP (WEB인 경우 토큰 유효시간 10시간, APPdms 1년)
    public LoginResponse doLogin(Admin admin, LoginRequest loginRequest, String loginType) {
        Rider rider = riderRepository.findByEmail(loginRequest.getEmail()).orElseThrow(CRiderMissingException::new);
        // 회원 정보가 없습니다 던지기

        if (!rider.getAdmin().equals(admin)) throw new CRiderMissingException();
        // 일반회원이 최고관리자용으로 로그인하려거나 이런 경우이므로 메세지는 회원정보가 없습니다로 일단 던짐.
        if (!passwordEncoder.matches(loginRequest.getPassword(), rider.getPassword())) throw new CRiderMissingException();
        // 비밀번호가 일치하지 않습니다 던짐

        String token = jwtTokenProvider.creatToken(String.valueOf(rider.getEmail()), rider.getAdmin().toString(), loginType);

        return new LoginResponse.Builder(token, rider.getName()).build();
    }
}