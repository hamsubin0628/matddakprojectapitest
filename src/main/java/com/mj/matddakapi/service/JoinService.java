package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.enums.rider.Admin;
import com.mj.matddakapi.exception.CRiderMissingException;
import com.mj.matddakapi.lib.CommonCheck;
import com.mj.matddakapi.model.rider.RiderJoinRequest;
import com.mj.matddakapi.repository.RiderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JoinService {
    private final RiderRepository riderRepository;
    private final PasswordEncoder passwordEncoder;

    public void setJoin(Admin admin, RiderJoinRequest riderJoinRequest) {
        if (!CommonCheck.checkEmail(riderJoinRequest.getEmail())) throw new CRiderMissingException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!riderJoinRequest.getPassword().equals(riderJoinRequest.getPasswordRe())) throw new CRiderMissingException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewRider(riderJoinRequest.getEmail())) throw new CRiderMissingException(); // 중복된 아이디가 존재합니다 던지기

        riderJoinRequest.setPassword(passwordEncoder.encode(riderJoinRequest.getPassword()));
        Rider rider = new Rider.Builder(riderJoinRequest, admin).build();
        riderRepository.save(rider);
    }

    private boolean isNewRider(String email) {
        long dupCount = riderRepository.countByEmail(email);
        return dupCount <= 0;
    }
}