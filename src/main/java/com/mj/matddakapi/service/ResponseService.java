package com.mj.matddakapi.service;

import com.mj.matddakapi.enums.ResultCode;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.generic.SingleResult;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResponseService {
//    public static CommonResult getSuccessResult() { //  C, U, D 입력하는 포트용
//        CommonResult result = new CommonResult();
//        result.setCode(ResultCode.SUCCESS.getCode());
//        result.setMsg(ResultCode.SUCCESS.getMsg());
//
//        return result;
//    }
//
//    public static <T> SingleResult<T> getSingleResult(T data) { // R 단수용
//        SingleResult<T> result = new SingleResult<>();
//        result.setCode(ResultCode.SUCCESS.getCode());
//        result.setMsg(ResultCode.SUCCESS.getMsg());
//        result.setData(data);
//
//        return result;
//    }

    public static <T> ListResult<T> getListResult(List<T> list) { // R 복수용
        ListResult<T> result = new ListResult<>();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
        result.setList(list);
        result.setTotalCount((long)list.size());

        return result;
    }

    public static <T> ListResult<T> getListResult(ListResult<T> result, boolean isSuccess) {
        if (isSuccess) setSuccessResult(result);
        else setFailResult(result);
        return result;
    }

    public static <T> SingleResult<T> getSingleResult(T data) {
        SingleResult<T> result = new SingleResult<>();
        result.setData(data);
        setSuccessResult(result);
        return result;
    }

    public static CommonResult getSuccessResult() {
        CommonResult result = new CommonResult();
        setSuccessResult(result);
        return result;
    }

    public static CommonResult getFailResult(ResultCode resultCode) {
        CommonResult result = new CommonResult();
        result.setIsSuccess(false);
        result.setCode(resultCode.getCode());
        result.setMsg(resultCode.getMsg());
        return result;
    }

    private static void setSuccessResult(CommonResult result) {
        result.setIsSuccess(true);
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
    }

    private static void setFailResult(CommonResult result) {
        result.setIsSuccess(false);
        result.setCode(ResultCode.FAILED.getCode());
        result.setMsg(ResultCode.FAILED.getMsg());
    }
}
