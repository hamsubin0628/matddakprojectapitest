package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Attendance;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.entity.RiderPay;
import com.mj.matddakapi.enums.attendance.StateWork;
import com.mj.matddakapi.enums.rider.Admin;
import com.mj.matddakapi.enums.rider.ReasonBan;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.rider.*;
import com.mj.matddakapi.model.rider.person.RiderBankChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderPersonChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderTypeChangeRequest;
import com.mj.matddakapi.repository.AttendanceRepository;
import com.mj.matddakapi.repository.RiderPayRepository;
import com.mj.matddakapi.repository.RiderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@RequiredArgsConstructor

public class RiderService {
    private final RiderRepository riderRepository;
    private final AttendanceRepository attendanceRepository;
    private final RiderPayRepository riderPayRepository;

    public Rider getData(long riderId) {
        return riderRepository.findById(riderId).orElseThrow();
    }

    /**
     * @param email 회원가입창에서 고객이 작성한 정보
     * @throws Exception 아이디 중복확인했을 때 데이터베이스에서 일치하는 이메일이 없을 경우
     */
    private boolean isNewRider(String email) {
        long dupCount = riderRepository.countByEmail(email);

        return dupCount <= 0;
    }

//    public void setRider(RiderJoinRequest request) throws Exception {
//        // 중복확인 로직
//        if (!isNewRider(request.getEmail())) throw new Exception();
//
//        // 15세 미만 예외처리
//        LocalDate today = LocalDate.now();
//        int birthYear = Integer.parseInt(request.getIdNum().substring(0, 2));
//        char generation = request.getIdNum().charAt(7);
//
//        int age = 0;
//        if (generation == '1' || generation == '2') {
//            age = today.getYear() - 1900 - birthYear;
//        } else if (generation == '3' || generation == '4') {
//            age = today.getYear() - 2000 - birthYear;
//        }
//
//        if (age < 15) throw new Exception();
//
//        Rider addData1 = new Rider.Builder(request).build();
//        riderRepository.save(addData1);
//
//        Attendance addData2 = new Attendance();
//        addData2.setRider(addData1);
//        addData2.setStateWork(StateWork.STOP_WORK);
//        addData2.setDateCheckWork(LocalDateTime.now());
//
//        attendanceRepository.save(addData2);
//
//        RiderPay addData3 = new RiderPay();
//        addData3.setRider(addData1);
//        addData3.setPayNow(0D);
//
//        riderPayRepository.save(addData3);
//    }

    /**
     * 관리자(웹) 회원 리스트 조회
     */
    public List<RiderItem> getRiders() {
        List<Rider> originList = riderRepository.findAll();
        List<RiderItem> result = new LinkedList<>();

        for (Rider rider : originList)
            result.add(new RiderItem.Builder(rider).build());

        return result;
    }

    /**
     * 관리자, 라이더: 회원 정보 조회 detail
     */
    public RiderResponse getRider(long id) {
        Rider originData = riderRepository.findById(id).orElseThrow();

        return new RiderResponse.Builder(originData).build();
    }

    /**
     * 관리자: 라이더 페이징 1~10페이지
     */
    public ListResult<RiderItem> getPages(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum - 1, 9);
        Page<Rider> riderPages = riderRepository.findAll(pageRequest);

        List<RiderItem> result = new LinkedList<>();

        for (Rider rider : riderPages.getContent())
            result.add(new RiderItem.Builder(rider).build());


        ListResult<RiderItem> result1 = new ListResult<>();
        result1.setList(result);
        result1.setTotalCount(riderPages.getTotalElements());
        result1.setTotalPage(riderPages.getTotalPages());
        result1.setCurrentPage(riderPages.getPageable().getPageNumber());

        return result1;
    }
    /**
     * 관리자: 회원정보 수정
     */
    public void putRiders(long id, RiderChangeRequest request){
        Rider originData = riderRepository.findById(id).orElseThrow();
        originData.putRiders(request);

        riderRepository.save(originData);
    }

    /**
     * 라이더: 개인정보 수정
     */
    public void putRider(long id, RiderPersonChangeRequest request){
        Rider originData = riderRepository.findById(id).orElseThrow();
        originData.putRider(request);

        riderRepository.save(originData);
    }
    /**
     * 라이더: 계좌 수정
     */
    public void putBank(long id, RiderBankChangeRequest request){
        Rider originData = riderRepository.findById(id).orElseThrow();
        originData.putBank(request);

        riderRepository.save(originData);
    }
    /**
     * 라이더: 운송수단 변경
     */
    public void putRidingType(long id, RiderTypeChangeRequest request){
        Rider originData = riderRepository.findById(id).orElseThrow();
        originData.putRidingType(request);

        riderRepository.save(originData);
    }
    public List<RiderItem> getBlackList(){
        List<Rider> originList = riderRepository.findAllByIsBan(true);
        List<RiderItem> result = new LinkedList<>();
        for (Rider rider : originList)
            result.add(new RiderItem.Builder(rider).build());

        return result;
    }
}
