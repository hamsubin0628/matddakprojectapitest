package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Store;
import com.mj.matddakapi.lib.CommonFile;
import com.mj.matddakapi.repository.StoreRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Locale;

@Service
@RequiredArgsConstructor
public class TempService {
    private final StoreRepository storeRepository;
    private Locale FilenameUtils;

    /**
     *
     * @param multipartFile 데이터 베이스에 가게 정보 한번에 업로드
     * @throws IOException
     */


    public void setStoreByFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.multipartToFile(multipartFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        // 버퍼에서 넘어온 string 한줄을 임시로 담아 둘 변수
        String line = "";
        // 줄 번호 수동체크 하기 위해 int로 줄 번호 1씩 증가해서 기록 해 둘 변수
        int index = 0;

        // 빈 값이 아닐 때까지 넣겠다
        while ((line = bufferedReader.readLine()) != null){
            if (index > 0) {
                String[] cols = line.split(",");
                if (cols.length == 6){

                    // db에 넣기
                    storeRepository.save(new Store.BuilderPer(cols).build());
                }
            }
            index++;
        }

        bufferedReader.close();
    }




    /**
     * 이미지 파일 업로드
     *
     * @param multipartFile
     * @return
     * @throws IOException
     */

    public File setUploadImage (MultipartFile multipartFile) throws IOException {
        File file = CommonFile.multipartToFile(multipartFile);
        multipartFile.transferTo(file);
        return file;
    }

    public static BufferedImage resizedImage (String image, int maxWidth, int maxHeight) throws IOException {
        File inputFile = new File(image);
        BufferedImage inputImage = ImageIO.read(inputFile);

        // 변환할 값, 비율
        int convertedWidth = 0;
        int convertedHeight = 0;
        float maxRatio = maxHeight / (float)maxWidth;
        // String ext = FilenameUtils.getExtension(image.getOriginalFilename());

        // 현재 이미지 정보, ratio : 비율
        int width = inputImage.getWidth();
        int height = inputImage.getHeight();
        float ratio = height / (float)width;

        // 이미지가 변환 사이즈 보다 클 경우 변환 진행
        if(width > maxWidth || height > maxHeight) {
            // 비율이 최대 비율 보다 작다면
            if(ratio < maxRatio) {
                convertedWidth = (int)(width * (maxWidth/(float)width));
                convertedHeight = (int)(height * (maxWidth/(float)width));
            } else { // 비율이 최대 비율 보다 작지 않다면
                convertedWidth = (int)(width * (maxHeight / (float)height));
                convertedHeight = (int)(height * (maxHeight / (float)height));
            }
        } else {
            return inputImage;
        }

        // 2차원 텍스트나 모양, 이미지를 렌더링 하기 위한 기본 클래스
        BufferedImage outputImage = new BufferedImage(convertedWidth, convertedHeight, inputImage.getType());

        Graphics2D graphics2D = outputImage.createGraphics();

        graphics2D.drawImage(inputImage, 0, 0, convertedWidth, convertedHeight, null);
        return outputImage;
    }
}
