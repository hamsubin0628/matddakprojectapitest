package com.mj.matddakapi.model.rider.login;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class LoginRequest {
    @NotNull
    @Length(min = 6, max = 40)
    private String Email;

    @NotNull
    @Length(min = 6, max = 20)
    private String Password;
}
