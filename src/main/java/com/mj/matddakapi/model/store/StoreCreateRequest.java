package com.mj.matddakapi.model.store;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreCreateRequest {
    private String storeName;
    private String addressStoreDo;
    private Double xStore;
    private Double yStore;
    private String addressStoreG;
    private String storeNumber;
    private String sellTime;
    private String holiday;
    private String deliveryArea;
    private String bossName;
    private String shopId;
}
