package com.mj.matddakapi.model.board;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardChangeRequest {
    private String title;
    private String img;
    private String text;
}
