package com.mj.matddakapi.model.income;

import com.mj.matddakapi.entity.Delivery;
import com.mj.matddakapi.entity.Income;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class IncomeItem {
    private Long id;
    private Long delivery;
    private Double feeRider;
    private Double feeAdmin;
    private Double feeTotal;
    private Double taxSan;
    private Double taxGo;
    private LocalDate dateIncome;
    private LocalDateTime timeIncome;

    public IncomeItem(Builder builder) {
        this.id = builder.id;
        this.delivery = builder.delivery;
        this.feeRider = builder.feeRider;
        this.feeAdmin = builder.feeAdmin;
        this.feeTotal = builder.feeTotal;
        this.taxSan = builder.taxSan;
        this.taxGo = builder.taxGo;
        this.dateIncome = builder.dateIncome;
        this.timeIncome = builder.timeIncome;
    }

    public static class Builder implements CommonModelBuilder<IncomeItem> {
        private final Long id;
        private final Long delivery;
        private final Double feeRider;
        private final Double feeAdmin;
        private final Double feeTotal;
        private final Double taxSan;
        private final Double taxGo;
        private final LocalDate dateIncome;
        private final LocalDateTime timeIncome;

        public Builder(Income income) {
            this.id = income.getId();
            this.delivery = income.getDelivery().getId();
            this.feeRider = income.getFeeRider();
            this.feeAdmin = income.getFeeAdmin();
            this.feeTotal = income.getFeeTotal();
            this.taxSan = income.getTaxSan();
            this.taxGo = income.getTaxGo();
            this.dateIncome = income.getDateIncome();
            this.timeIncome = income.getTimeIncome();
        }
        @Override
        public IncomeItem build() {
            return new IncomeItem(this);
        }
    }
}