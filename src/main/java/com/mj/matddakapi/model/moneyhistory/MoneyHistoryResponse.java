package com.mj.matddakapi.model.moneyhistory;

import com.mj.matddakapi.entity.MoneyHistory;
import com.mj.matddakapi.enums.moneyHistory.MoneyType;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyHistoryResponse {
    private Long id;
    private Long riderId;
    private String riderName;
    private String moneyType;
    private Double moneyAmount;
    private LocalDateTime dateMoneyRenewal;
    private String etcMemo;

    public MoneyHistoryResponse(Builder builder) {
        this.id = builder.id;
        this.riderId = builder.riderId;
        this.riderName = builder.riderName;
        this.moneyType = builder.moneyType;
        this.moneyAmount = builder.moneyAmount;
        this.dateMoneyRenewal = builder.dateMoneyRenewal;
        this.etcMemo = builder.etcMemo;
    }

    public static class Builder implements CommonModelBuilder<MoneyHistoryResponse> {
        private final Long id;
        private final Long riderId;
        private final String riderName;
        private final String moneyType;
        private final Double moneyAmount;
        private final LocalDateTime dateMoneyRenewal;
        private final String etcMemo;

        public Builder(MoneyHistory moneyHistory) {
            this.id = moneyHistory.getId();
            this.riderId = moneyHistory.getRider().getId();
            this.riderName = moneyHistory.getRider().getName();
            this.moneyType = moneyHistory.getMoneyType().getName();
            this.moneyAmount = moneyHistory.getMoneyAmount();
            this.dateMoneyRenewal = moneyHistory.getDateMoneyRenewal();
            this.etcMemo = moneyHistory.getEtcMemo();
        }

        @Override
        public MoneyHistoryResponse build() {
            return new MoneyHistoryResponse(this);
        }
    }
}
