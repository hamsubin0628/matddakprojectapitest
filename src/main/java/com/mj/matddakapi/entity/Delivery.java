package com.mj.matddakapi.entity;

import com.mj.matddakapi.enums.delivery.StateDelivery;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import jakarta.persistence.*;
import lombok.*;

import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Delivery {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER) // lazy 확인
    @JoinColumn(name = "riderId", nullable = false)
    private Rider rider;

    @ManyToOne(fetch = FetchType.EAGER) // lazy 확인
    @JoinColumn(name = "askId", nullable = false)
    private Ask ask;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private StateDelivery stateDelivery;

    @Column(nullable = false)
    private String datePick;

    private String dateGo;

    private String dateDone;

    private String dateCancel;

    public void putCancel() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date today = new Date();
        
        this.stateDelivery = StateDelivery.R_CANCEL;
        this.dateCancel = dateFormat.format(today);
    }

    public void putDone() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date today = new Date();
        
        this.stateDelivery = StateDelivery.DONE;
        this.dateDone = dateFormat.format(today);
    }

    public void putGo() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date today = new Date();
        
        this.stateDelivery = StateDelivery.GO;
        this.dateGo = dateFormat.format(today);
    }

    public Delivery(Builder builder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date today = new Date();

        this.rider = builder.rider;
        this.ask = builder.ask;
        this.stateDelivery = builder.stateDelivery;
        this.datePick = dateFormat.format(today);
    }

    public static class Builder implements CommonModelBuilder<Delivery> {
        private final Rider rider;
        private final Ask ask;
        private final StateDelivery stateDelivery;
        private final String datePick;
        private String dateGo;
        private String dateDone;
        private String dateCancel;

        public Builder(Rider rider, Ask ask) {
            this.rider = rider;
            this.ask = ask;
            this.stateDelivery = StateDelivery.R_PICK;
            this.datePick = toString();
        }

        @Override
        public Delivery build() {
            return new Delivery(this);
        }
    }
}
