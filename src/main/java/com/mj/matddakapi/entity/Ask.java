package com.mj.matddakapi.entity;

import com.mj.matddakapi.enums.ask.HowPay;
import com.mj.matddakapi.enums.ask.StateAsk;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import com.mj.matddakapi.model.ask.AskCreateRequest;
import jakarta.persistence.*;
import lombok.*;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Ask {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER) // Lazy 가능한지 확인
    @JoinColumn(name = "storeId", nullable = false)
    private Store store;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private StateAsk stateAsk;

    @Column(nullable = false)
    private LocalDate dateAsk;

    @Column(nullable = false)
    private String timeAsk;

    @Column(nullable = false, length = 50)
    private String askMenu;

    @Column(nullable = false)
    private Double priceTotal;

    @Column(nullable = false)
    private Double priceFood;

    @Column(nullable = false)
    private Double priceRide;

    @Column(nullable = false)
    private Boolean isPay;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private HowPay howPay;

    @Column(columnDefinition = "TEXT")
    private String requestStore;

    @Column(columnDefinition = "TEXT")
    private String requestRider;

    @Column(nullable = false, length = 15)
    private String phoneClient;

    @Column(nullable = false, length = 100)
    private String addressClientDo;

    @Column(length = 100)
    private String addressClientG;

    @Column(nullable = false)
    private Double xClient;

    @Column(nullable = false)
    private Double yClient;

    @Column(nullable = false)
    private Double distance;


    public void putAskPick() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date today = new Date();

        this.stateAsk = StateAsk.S_PICK;
        this.dateAsk = LocalDate.now();
        this.timeAsk = dateFormat.format(today);
    }

    public void putAskCancel() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date today = new Date();

        this.stateAsk = StateAsk.S_CANCEL;
        this.dateAsk = LocalDate.now();
        this.timeAsk = dateFormat.format(today);
    }

    public Ask(Builder builder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date today = new Date();

        this.store = builder.store;
        this.stateAsk = builder.stateAsk;
        this.dateAsk = builder.dateAsk;
        this.timeAsk = dateFormat.format(today);
        this.askMenu = builder.askMenu;
        this.priceTotal = builder.priceTotal;
        this.priceFood = builder.priceFood;
        this.priceRide = builder.priceRide;
        this.isPay = builder.isPay;
        this.howPay = builder.howPay;
        this.requestStore = builder.requestStore;
        this.requestRider = builder.requestRider;
        this.phoneClient = builder.phoneClient;
        this.addressClientDo = builder.addressClientDo;
        this.addressClientG = builder.addressClientG;
        this.xClient = builder.xClient;
        this.yClient = builder.yClient;
        this.distance = builder.distance;
    }

    public static class Builder implements CommonModelBuilder<Ask> {
        private final Store store;
        private final StateAsk stateAsk;
        private final LocalDate dateAsk;
        private final String timeAsk;
        private final String askMenu;
        private final Double priceTotal;
        private final Double priceFood;
        private final Double priceRide;
        private final Boolean isPay;
        private final HowPay howPay;
        private final String requestStore;
        private final String requestRider;
        private final String phoneClient;
        private final String addressClientDo;
        private final String addressClientG;
        private final Double xClient;
        private final Double yClient;
        private final Double distance;

        public Builder(Store store, AskCreateRequest request) {
            double distance1;

            this.store = store;
            this.stateAsk = StateAsk.REQUESTING;
            this.dateAsk = LocalDate.now();
            this.timeAsk = toString();
            this.askMenu = request.getAskMenu();
            this.priceFood = request.getPriceFood();
            this.priceRide = request.getPriceRide();
            this.priceTotal = priceFood+priceRide;
            this.isPay = request.getIsPay();
            this.howPay = request.getHowPay();
            this.requestStore = request.getRequestStore();
            this.requestRider = request.getRequestRider();
            this.phoneClient = request.getPhoneClient();
            this.addressClientDo = request.getAddressClientDo();
            this.addressClientG = request.getAddressClientG();
            this.xClient = request.getXClient();
            this.yClient = request.getYClient();
            distance1 = Math.round(Math.sqrt(Math.pow((xClient - store.getXStore()), 2) + Math.pow((yClient - store.getYStore()), 2)) * 98.8368 * 10) / 10.0;
            if (distance1 > 10) {
                distance1 = distance1 - 3;
            }
            this.distance = distance1;
        }

        @Override
        public Ask build() {
            return new Ask(this);
        }
    }
}