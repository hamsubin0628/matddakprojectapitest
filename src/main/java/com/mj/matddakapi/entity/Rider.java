package com.mj.matddakapi.entity;

import com.mj.matddakapi.enums.rider.*;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import com.mj.matddakapi.model.rider.RiderChangeRequest;
import com.mj.matddakapi.model.rider.RiderJoinRequest;
import com.mj.matddakapi.model.rider.person.RiderBankChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderPersonChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderTypeChangeRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Rider implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private Admin admin;

    @Column(nullable = false, length = 14)
    private String idNum;

    @Column(length = 100)
    private String confirmParents;

    @Column(nullable = false, unique = true, length = 40)
    private String email;

    @Column(nullable = false, length = 20)
    private String password;

    @Column(nullable = false, unique = true, length = 20)
    private String phoneNumber;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private PhoneType phoneType;

    @Column(nullable = false, length = 20)
    private String bankOwner;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private BankName bankName;

    @Column(nullable = false, length = 14)
    private String bankIdNum;

    @Column(nullable = false, length = 30)
    private String bankNumber;

    @Column(nullable = false, length = 30)
    @Enumerated(value = EnumType.STRING)
    private AddressWish addressWish;

    @Column(nullable = false, length = 50)
    @Enumerated(value = EnumType.STRING)
    private DriveType driveType;

    @Column(length = 20)
    private String driveNumber;

    @Column(nullable = false)
    private LocalDate dateJoin;

    @Column(nullable = false)
    private Boolean isBan;

    @Enumerated(value = EnumType.STRING)
    @Column(length = 50)
    private ReasonBan reasonBan;

    @Column(length = 20)
    private String dateBan;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;

    @Column(nullable = false)
    private Boolean agPrivacy;

    @Column(nullable = false)
    private Boolean agLocation;


    /**
     * 관리자: 회원정보 수정
     */
    public void putRiders(RiderChangeRequest request) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date today = new Date();

        this.admin = request.getAdmin();
        this.email = request.getEmail();
        this.phoneNumber = request.getPhoneNumber();
        this.phoneType = request.getPhoneType();
        this.bankName = request.getBankName();
        this.bankIdNum = request.getBankIdNum();
        this.bankOwner = request.getBankOwner();
        this.bankNumber = request.getBankNumber();
        this.addressWish = request.getAddressWish();
        this.driveType = request.getDriveType();
        this.driveNumber = request.getDriveNumber();
        this.isBan = request.getIsBan();
        this.reasonBan = request.getReasonBan();
        this.dateBan = dateFormat.format(today);
        this.etcMemo = request.getEtcMemo();
    }

    /**
     * 라이더: 개인정보 수정
     */
    public void putRider(RiderPersonChangeRequest request) {
        this.email = request.getEmail();
        this.password = request.getPassword();
        this.phoneNumber = request.getPhoneNumber();
        this.addressWish =request.getAddressWish();
    }

    /**
     * 라이더 : 계좌 수정
     */
    public void putBank(RiderBankChangeRequest request) {
        this.bankOwner = request.getBankOwner();
        this.bankName = request.getBankName();
        this.bankIdNum = request.getBankIdNum();
        this.bankNumber = request.getBankNumber();
    }

    public void putRidingType(RiderTypeChangeRequest request) {
        this.driveType = request.getDriveType();
        this.driveNumber = request.getDriveNumber();
    }



    public Rider(Builder builder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date today = new Date();

        this.name = builder.name;
        this.admin = builder.admin;
        this.idNum = builder.idNum;
        this.confirmParents = builder.confirmParents;
        this.email = builder.email;
        this.password = builder.password;
        this.phoneNumber = builder.phoneNumber;
        this.phoneType = builder.phoneType;
        this.bankOwner = builder.bankOwner;
        this.bankName = builder.bankName;
        this.bankIdNum = builder.bankIdNum;
        this.bankNumber = builder.bankNumber;
        this.addressWish = builder.addressWish;
        this.driveType = builder.driveType;
        this.driveNumber = builder.driveNumber;
        this.dateJoin = builder.dateJoin;
        this.isBan = builder.isBan;
        this.reasonBan = builder.reasonBan;
        this.dateBan = dateFormat.format(today);
        this.etcMemo = builder.etcMemo;
        this.agPrivacy = builder.agPrivacy;
        this.agLocation = builder.agLocation;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(admin.toString()));
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    public static class Builder implements CommonModelBuilder<Rider> {
        private final String name;
        private final Admin admin;
        private final String idNum;
        private final String confirmParents;
        private final String email;
        private final String password;
        private final String phoneNumber;
        private final PhoneType phoneType;
        private final String bankOwner;
        private final BankName bankName;

        private final String bankIdNum;

        private final String bankNumber;
        private final AddressWish addressWish;
        private final DriveType driveType;
        private final String driveNumber;
        private final LocalDate dateJoin;
        private final Boolean isBan;
        private final ReasonBan reasonBan;

        private  String etcMemo;

        private String dateBan;

        private final Boolean agPrivacy;
        private final Boolean agLocation;

        public Builder(RiderJoinRequest request, Admin admin) {
            LocalDate today = LocalDate.now();

            /**
             * 주민번호 입력시 생년월일로 치환하는 로직 s
             */

            int birthYear = Integer.parseInt(request.getIdNum().substring(0, 2));
            char generation = request.getIdNum().charAt(7);

            int age = 0;
            if (generation == '1' || generation == '2') {
                age = today.getYear() - 1900 - birthYear;
            } else if (generation == '3' || generation == '4') {
                age = today.getYear() - 2000 - birthYear;
            }

            this.name = request.getName();
            this.admin = admin;
            this.idNum = request.getIdNum();

            if (age < 18) { // 나이가 18세 미만 일때 실행
                this.confirmParents = request.getConfirmParents();
                this.isBan = true;
                this.reasonBan = ReasonBan.PARENTAL_CONSENT;
                this.dateBan = today.toString();
            } else {
                this.confirmParents = request.getConfirmParents();
                this.isBan = false;
                this.reasonBan = ReasonBan.NOT_BAN;
            }

            this.email = request.getEmail();
            this.password = request.getPassword();
            this.phoneNumber = request.getPhoneNumber();
            this.phoneType = request.getPhoneType();
            this.bankOwner = request.getBankOwner();
            this.bankName = request.getBankName();
            this.bankIdNum = request.getBankIdNum();
            this.bankNumber = request.getBankNumber();
            this.addressWish = request.getAddressWish();
            this.driveType = request.getDriveType();
            this.driveNumber = request.getDriveNumber();
            this.dateJoin = LocalDate.now();
            this.agPrivacy = request.getAgPrivacy();
            this.agLocation = request.getAgLocation();
        }
        @Override
        public Rider build() {
            return new Rider(this);
        }
    }
}

