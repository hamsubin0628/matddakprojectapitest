package com.mj.matddakapi.entity;

import com.mj.matddakapi.enums.attendance.StateWork;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Attendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER) // Lazy 바꿔서 가능한지 확인
    @JoinColumn(name = "riderId", nullable = false)
    private Rider rider;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private StateWork stateWork;

    @Column(nullable = false)
    private LocalDateTime dateCheckWork;
}
