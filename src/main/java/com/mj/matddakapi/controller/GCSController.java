package com.mj.matddakapi.controller;

import com.mj.matddakapi.model.GCSRequest;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.service.GCSService;
import com.mj.matddakapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;

import java.io.IOException;

@Controller
@RequiredArgsConstructor
@RequestMapping("/v1/gcs")
public class GCSController {

    private final GCSService gcsService;

    @PostMapping("/upload")
    @Operation(summary = "GCS 파일 업로드")
    public CommonResult objectUpload(@RequestPart GCSRequest gcsRequest) throws IOException {
        gcsService.uploadObject(gcsRequest);
        return ResponseService.getSuccessResult();
    }
}
