package com.mj.matddakapi.controller;

import com.mj.matddakapi.entity.Ask;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.model.delivery.DeliveryItem;
import com.mj.matddakapi.model.delivery.DeliveryResponse;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.service.AskService;
import com.mj.matddakapi.service.DeliveryService;
import com.mj.matddakapi.service.ResponseService;
import com.mj.matddakapi.service.RiderService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/delivery")
public class DeliveryController {
    private final DeliveryService deliveryService;
    private final RiderService riderService;
    private final AskService askService;

    @PostMapping("/new/rider-id/{riderId}/ask-id/{askId}")
    @Operation(summary = "배차 신청")
    public CommonResult setDelivery(@PathVariable long riderId, @PathVariable long askId) throws Exception {
        Rider rider = riderService.getData(riderId);
        Ask ask = askService.getData(askId);
        deliveryService.setDelivery(rider, ask);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/change/go/{deliveryId}")
    @Operation(summary = "출발 상태로 변경")
    public CommonResult putGo(@PathVariable long deliveryId) {
        deliveryService.putGo(deliveryId);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/change/done/{deliveryId}")
    @Operation(summary = "완료 상태로 변경")
    public CommonResult putDone(@PathVariable long deliveryId) {
        deliveryService.putDone(deliveryId);

        return ResponseService.getSuccessResult();
    }

//    @PutMapping("/change/cancel/{deliveryId}")
//    @Operation(summary = "취소 상태로 변경")
//    public CommonResult putCancel(@PathVariable long deliveryId) {
//        deliveryService.putCancel(deliveryId);
//
//        return ResponseService.getSuccessResult();
//    }

    @GetMapping("/all")
    @Operation(summary = "배송 복수 최신순")
    public ListResult<DeliveryItem> getDeliveries() {
        return ResponseService.getListResult(deliveryService.getDeliveries(), true);
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "배송 복수 최신순 페이징")
    public ListResult<DeliveryItem> getDeliveriesP(@PathVariable int pageNum) {
        return ResponseService.getListResult(deliveryService.getDeliveriesP(pageNum), true);
    }

    @GetMapping("/all/pick")
    @Operation(summary = "배송 복수 배차만 최신순")
    public ListResult<DeliveryItem> getPick() {
        return ResponseService.getListResult(deliveryService.getPick(), true);
    }

    @GetMapping("/all/pick/{pageNum}")
    @Operation(summary = "배송 복수 배차만 최신순 페이징")
    public ListResult<DeliveryItem> getPickP(@PathVariable int pageNum) {
        return ResponseService.getListResult(deliveryService.getPickP(pageNum), true);
    }

    @GetMapping("/all/go")
    @Operation(summary = "배송 복수 출발만 최신순")
    public ListResult<DeliveryItem> getGo() {
        return ResponseService.getListResult(deliveryService.getGo(), true);
    }

    @GetMapping("/all/go/{pageNum}")
    @Operation(summary = "배송 복수 출발만 최신순 페이징")
    public ListResult<DeliveryItem> getGoP(@PathVariable int pageNum) {
        return ResponseService.getListResult(deliveryService.getGoP(pageNum), true);
    }

    @GetMapping("/all/done")
    @Operation(summary = "배송 복수 완료만 최신순")
    public ListResult<DeliveryItem> getDone() {
        return ResponseService.getListResult(deliveryService.getDone(), true);
    }

    @GetMapping("/all/done/{pageNum}")
    @Operation(summary = "배송 복수 완료만 최신순 페이징")
    public ListResult<DeliveryItem> getDoneP(@PathVariable int pageNum) {
        return ResponseService.getListResult(deliveryService.getDoneP(pageNum), true);
    }
//    @GetMapping("/all/cancel")
//    @Operation(summary = "배송 복수 취소만 최신순")
//    public ListResult<DeliveryItem> getCancel() {
//        return ResponseService.getListResult(deliveryService.getCancel(), true);
//    }
//
//    @GetMapping("/all/cancel/{pageNum}")
//    @Operation(summary = "배송 복수 취소만 최신순 페이징")
//    public ListResult<DeliveryItem> getCancelP(@PathVariable int pageNum) {
//        return ResponseService.getListResult(deliveryService.getCancelP(pageNum), true);
//    }

    @GetMapping("/detail/{deliveryId}")
    @Operation(summary = "배송 단수")
    public SingleResult<DeliveryResponse> getDelivery(@PathVariable long deliveryId) {
        return ResponseService.getSingleResult(deliveryService.getDelivery(deliveryId));
    }
}
