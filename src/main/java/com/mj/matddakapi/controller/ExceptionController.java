package com.mj.matddakapi.controller;

import com.mj.matddakapi.exception.CRiderMissingException;
import com.mj.matddakapi.model.generic.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {

    @GetMapping("/access-denied")
    public CommonResult CMonsterFullException() { throw new CRiderMissingException(); }
       // ex) 강의실 사용하는 사람만 앉을 수 있는 의자

    @GetMapping("/entry-point")
    public CommonResult entryPointException() { throw new CRiderMissingException(); }
        // ex) 나만 앉을 수 있는 내 자리 의자
}
