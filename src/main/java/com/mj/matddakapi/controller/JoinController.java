package com.mj.matddakapi.controller;

import com.mj.matddakapi.enums.rider.Admin;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.model.rider.RiderJoinRequest;
import com.mj.matddakapi.service.JoinService;
import com.mj.matddakapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/join")
public class JoinController {
    private final JoinService joinService;

    @PostMapping("/admin")
    @Operation(summary = "관리자 등록")
    public CommonResult setJoin(@RequestBody RiderJoinRequest riderJoinRequest) throws Exception {
        joinService.setJoin(Admin.ROLE_ADMIN, riderJoinRequest);

        return ResponseService.getSuccessResult();
    }

    @PostMapping("/rider")
    @Operation(summary = "라이더 등록")
    public CommonResult setRider(@RequestBody RiderJoinRequest riderJoinRequest) throws Exception {
        joinService.setJoin(Admin.ROLE_NORMAL, riderJoinRequest);

        return ResponseService.getSuccessResult();
    }
}
