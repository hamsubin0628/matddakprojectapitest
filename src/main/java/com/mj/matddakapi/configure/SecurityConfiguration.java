package com.mj.matddakapi.configure;

import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {
    private final JwtTokenProvider jwtTokenProvider;
    private final CustomAuthenticationEntryPoint entryPoint;
    private final CustomAccessDeniedHandler accessDenied;

    @Bean // 왜 Bean일까 => 처음부터 설정되어야 하니까
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();

        config.setAllowCredentials(true);
        config.setAllowedOrigins(List.of("*"));
        config.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS")); // Mapping 종류, *OPTIONS 한 요청 당 2번씩 감 프리플라이트 Mapping => OPTIONS
        config.setAllowedHeaders(List.of("*")); // 헤더 다 뚫어 줘야함
        config.setExposedHeaders(List.of("*")); // 노출된 헤더 다 허용

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config); // 모든 맵핑 주소에 대해서 위 설정을 먹이겠다.
        return source;
    }

    /*
    flutter 에서 header에 token 넣는 법
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = ''Bearer ' + token!; <-- 한줄 추가
    */

    /*
    vue.js에서 axios에 header 넣는 법
    https://velog.io/@ch9eri/Axios-headers-Authorization
    */

    // XSS = PHP 세선 탈취 => csrf
    // SOP => cors

    @Bean
    protected SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf(AbstractHttpConfigurer::disable) // csrf를 비활성화 해라 => 토큰을 쓰기 때문에, 쿠키와 세션을 쓰지 않기 때문에 csrf를 꺼야함
                .formLogin(AbstractHttpConfigurer::disable)
                .httpBasic(AbstractHttpConfigurer::disable) // 기본 웹이냐, 만들지도 않은 로그인 창
                .cors(corsConfig -> corsConfig.configurationSource(corsConfigurationSource())) // 이때 풀어주는 것
                .sessionManagement((sessionManagement) ->
                        sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS)) // STATELESS 값 없음 => 세션 안쓴다
                .authorizeHttpRequests((authorizeRequests) -> // 인증 : 나라는 것을 확인하는 것 / 인가 : 인증 요청을 허가하는 것
                        authorizeRequests // authorize : 인가
                                .requestMatchers("/v3/**", "swagger-ui/**").permitAll()
                                .requestMatchers(HttpMethod.GET, "/exception/**").permitAll() // 모든곳 뚫어줌
                                .requestMatchers("/v1/member/login/**").permitAll() // 로그인 일단 풀어줌
                                .requestMatchers("/vi/auth-test/test-admin").hasAnyRole("MEMBER", "ADMIN") // 멤버와 관리자에게 권한을 줌
                                .anyRequest().hasRole("ADMIN")
                        );

        http.exceptionHandling(handler -> handler.accessDeniedHandler(accessDenied)); // exceptionHandling => 예외처리 잘못들어왔다 메세지 보여줘야 하니까
        http.exceptionHandling(handler -> handler.authenticationEntryPoint(entryPoint));
        http.addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
        // 필더링 진행하기 전에 .class : 필터의 자리

        return http.build();
    }

    @Bean
    public Storage storage() {
        return StorageOptions.getDefaultInstance().getService();
    }
}
