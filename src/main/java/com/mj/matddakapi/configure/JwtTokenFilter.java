package com.mj.matddakapi.configure;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtTokenFilter extends GenericFilterBean {
    // GenericFilterBean : 필터 종류를 Bean( 시작할때 설정 값 )에 등록한다
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            // chain : 필터 단계
            throws IOException, ServletException {
        String tokenFull = jwtTokenProvider.resolveToken((HttpServletRequest) request);
        String token = "";
        if (tokenFull == null || !tokenFull.startsWith("Bearer ")) {
            chain.doFilter(request, response);
            return; // void인데 왜 return? => 걍 끝남
        }
        token = tokenFull.split(" ")[1].trim(); // [1] 첫 번째 요소, trim : 앞 뒤 공백을 제거 해줌 ( 혹시 모를 에러 방지기 )
        if (!jwtTokenProvider.validateToken(token)) {
            chain.doFilter(request, response);
            return;
        }
        Authentication authentication = jwtTokenProvider.getAuthentication(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
        // Context : 환경, holder : 고정시키는 것 => 1회용 출입증을 고정시킬 수 있는 곳에 잠시 고정 ( 왔다 갔다 하면 유실되니까 )
    }
}
