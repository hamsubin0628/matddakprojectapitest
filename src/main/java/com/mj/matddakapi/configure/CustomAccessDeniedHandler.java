package com.mj.matddakapi.configure;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        // servlet => 서빙 => Http를 서빙 => HttpServletRequest : 고객이 요청하는 데이터를 서빙하면서 들고 다니는 애
        // HttpServletResponse : 서버가 고객한테 들고 가는 값
        response.sendRedirect("/exception/access-denied");
    }
    // 상황에 대해서 조종해줌
}
