package com.mj.matddakapi.configure;

import com.mj.matddakapi.enums.ResultCode;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.service.ResponseService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionAdvice {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e){
        return ResponseService.getFailResult(ResultCode.FAILED);
    }
}
