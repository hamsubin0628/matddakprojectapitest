package com.mj.matddakapi.configure;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class JwtTokenProvider { // provider : 제공하디, 검사하다
    private final UserDetailsService userDetailsService;
    @Value("${spring.jwt.secret}")
    private String secretKey;

    // Post : 먼저, Construct : 구조 ( ts 구조체 ) => 초기화 => 본격적으로 하기 전에 준비하는 것
    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
        // Base64 => 옛날 암호화 기법 ( 단방향 암호화 )
    }

    // 토큰 생성
    // claim : 주장하다 ( 보안, 토큰, 주장 ) => " 나야 "
    public String creatToken(String username, String role, String type) {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("role", role);
        Date now = new Date(); // 외국 로그인 고려
        // 토큰 유효시간
        // 1000 밀리세컨드 = 1초
        // 기본으로 10시간 유효하게 설정해 줌. 왜냐, 아침에 출근해서 로그인하고 점심먹고 퇴근하면 대충 10시간이니까.
        // 앱용 토큰 같은 경우 유효시간 1년으로 설정해 줌. 앱에서 아침마다 로그인하라고 하면 짜증나니까.
        long tokenValidMillisecond = 1000L * 60 * 60 * 10; // 백에서 10시간을 지정해 줬으니 프론트도 웹 토큰 쿠키 만료 시간도 10시간으로 지정해야함
        if (type.equals("APP")) tokenValidMillisecond = 1000L * 60 * 60 * 24 * 365;
        //토큰 생성해서 리턴.
        // jwt 사이트 참고.
        // 유효시간도 넣어줌. 생성요청한 시간 ~ 현재 + 위에서 설정된 유효초 만큼.
        return Jwts.builder()
                .setClaims(claims) // 나야
                .setIssuedAt(now) // 언제 발급됐어
                .setExpiration(new Date(now.getTime() + tokenValidMillisecond))  // 언제 얘가 파기된다. ( 만료일 )
                .signWith(SignatureAlgorithm.HS256,secretKey) // 싸인
                .compact(); // 작은
    }

    // 토큰을 분석하여 인증정보를 가져옴.
    public Authentication getAuthentication (String token) { // Authentication : 1회용 출입증
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }
    // principal : 중요한
    // credential : 신원정보

    // 토큰을 파싱하여 username을 가져옴.
    // 토큰 생성시 username은 subject에 넣은 것 꼭 확인.
    // jwt 사이트 보면서 코드 이해하기.
    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    //리졸브(resolve)라는 단어도 많이 씀. 결정, 해결
    public String resolveToken (HttpServletRequest request) {
        // rest api - header 인증 방식에서 Bearder를 언제 사용하는지 보기.
        return request.getHeader(HttpHeaders.AUTHORIZATION); //
        // promise : 실패한 값, 성공한 값 모두가 promise ( 성공 여부를 떠난 받은 결과물 )
        // resolve : 무조건 성공한 값 ( 내가 받은 성공 결과물 )
    }

    // 유효시간을 검사하여 유효시간이 지났으면 false를 줌.
    public boolean validateToken (String jwtToken) {
        try { // try, catch 이유 => 여기서 throw 처리하면 유지보수에 문제가 생김
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwtToken);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (Exception e) {
            return false;
        }
    }
}
